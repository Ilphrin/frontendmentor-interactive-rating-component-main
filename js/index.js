window.onload = () => {
  let selectedRate = null;

  document.querySelectorAll("#rates span").forEach((span, index) => {
    span.addEventListener("click", () => {
      selectedRate = index + 1;
    });
  });

  document.querySelector("button[type='submit']").addEventListener("click", () => {
    document.querySelector("#rating").className = "inactive";
    document.querySelector("#selectedRate").innerText = selectedRate;
    document.querySelector("#rated").className = "active";
  })
};
